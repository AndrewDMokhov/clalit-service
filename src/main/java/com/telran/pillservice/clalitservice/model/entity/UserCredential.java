package com.telran.pillservice.clalitservice.model.entity;


import com.telran.pillservice.clalitservice.model.base.AbstractBaseEntity;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "USER_CREDENTIAL")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class UserCredential extends AbstractBaseEntity {
    @OneToOne
    @JoinColumn(name = "USER_ID", nullable = false, unique = true)
    private User user;

    @Column(name = "PASSWORD_HASH", nullable = false)
    @Lob
    private String passwordHash;

    @Column(name = "PASSWORD_SALT", nullable = false)
    @Lob
    private String passwordSalt;




}
