package com.telran.pillservice.clalitservice.model.base;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@NoArgsConstructor
@Getter
@Setter
public abstract class AbstractBasePerson extends AbstractBaseEntity {

    @Column(name="USER_ID", nullable = false)
    private String userIdentificationNumber;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column (name = "LAST_NAME")
    private String  lastName;
}
