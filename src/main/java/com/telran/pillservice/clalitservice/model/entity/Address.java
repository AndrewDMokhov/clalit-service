package com.telran.pillservice.clalitservice.model.entity;

import com.telran.pillservice.clalitservice.model.base.AbstractBaseEntity;
import lombok.*;
import javax.persistence.*;

@Entity
@Table(name = "ADDRESSES")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Address extends AbstractBaseEntity {

    @OneToOne
    @JoinColumn(name = "ID")
    private User userId;

    @Column(name = "CITY")
    private String city;

    @Column(name = "STREET")
    private String street;

    @Column(name = "BUILD_NUM")
    private String buildNumber;

    @Column(name = "APARTMENT_NUM")
    private String apartmentNumber;
}
