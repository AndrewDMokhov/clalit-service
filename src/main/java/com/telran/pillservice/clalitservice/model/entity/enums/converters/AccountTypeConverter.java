package com.telran.pillservice.clalitservice.model.entity.enums.converters;

import com.telran.pillservice.clalitservice.model.entity.enums.AccountStatus;
import com.telran.pillservice.clalitservice.model.entity.enums.AccountType;

import javax.persistence.AttributeConverter;
import javax.persistence.Convert;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class AccountTypeConverter implements AttributeConverter<AccountType, Integer> {
    @Override
    public Integer convertToDatabaseColumn(AccountType accountType) {
        return accountType == null ? AccountType.REGULAR.getId() : accountType.getId();
    }

    @Override
    public AccountType convertToEntityAttribute(Integer id) {
        return id == null ? AccountType.REGULAR : AccountType.getById(id);
    }
}
