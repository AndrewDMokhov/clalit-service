package com.telran.pillservice.clalitservice.model.entity.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;

import java.util.Arrays;

@AllArgsConstructor
public enum UserType {
    GUEST(1),
    REGULAR(2),
    ADMIN(3);


    private Integer id;

    @JsonValue
    public Integer getId() {
        return id;
    }

    @JsonCreator
    public static UserType getById(Integer id) {
        if(id == null) {
            return UserType.GUEST;
        }

        return Arrays.stream(values())
                .filter(x -> x.getId().equals(id))
                .findFirst()
                .orElse(null);
    }
}
