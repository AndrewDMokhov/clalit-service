package com.telran.pillservice.clalitservice.model.entity.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;

import java.util.Arrays;

@AllArgsConstructor
public enum  Gender {
    MALE(1),
    FEMALE(2),
    INDEFINITE(3);

    private Integer id;

    @JsonValue
    public Integer getId() {
        return id;
    }

    @JsonCreator
    public static Gender getById(Integer id) {
        if (id == null) return Gender.INDEFINITE;

        return Arrays.stream(values())
                .filter(x -> x.getId().equals(id))
                .findFirst()
                .orElse(Gender.INDEFINITE);
    }
}
