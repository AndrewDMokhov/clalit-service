package com.telran.pillservice.clalitservice.model.entity;

import com.telran.pillservice.clalitservice.model.base.AbstractBaseEntity;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.stereotype.Indexed;

import javax.persistence.*;

import java.util.Date;
@Entity
@Table(name = "EMAILS_CONFIRMATION")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@Indexed
public class EmailConfirmation extends AbstractBaseEntity {

    @ManyToOne
    @JoinColumn(name = "USER_ID", nullable = false)
    private User user;


    @Column(name = "CONFIRM_KEY")
    private String confirmKey;

    @CreationTimestamp
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date confirmCreateTime;
}
