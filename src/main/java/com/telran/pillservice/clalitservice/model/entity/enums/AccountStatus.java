package com.telran.pillservice.clalitservice.model.entity.enums;

import lombok.AllArgsConstructor;

import java.util.Arrays;

@AllArgsConstructor
public enum AccountStatus {
    NEW(1),
    CONFIRMATION(2),
    BLOCKED(3);

    private Integer id;

    public Integer getId(){ return id;}

    public static AccountStatus getById(Integer id){
        if(id == null){
            return null;
        }
        return Arrays.stream(values())
                .filter(x -> x.equals(id))
                .findFirst()
                .orElse(null);
    }
}
