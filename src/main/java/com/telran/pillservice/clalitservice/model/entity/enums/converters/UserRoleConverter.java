package com.telran.pillservice.clalitservice.model.entity.enums.converters;

import com.telran.pillservice.clalitservice.model.entity.enums.UserRole;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class UserRoleConverter implements AttributeConverter<UserRole, Integer> {

    @Override
    public Integer convertToDatabaseColumn(UserRole userRole) {
        return userRole == null ? UserRole.GUEST.getId() : userRole.getId();
    }

    @Override
    public UserRole convertToEntityAttribute(Integer id) {
        return id == null ? UserRole.GUEST : UserRole.getById(id);
    }
}
