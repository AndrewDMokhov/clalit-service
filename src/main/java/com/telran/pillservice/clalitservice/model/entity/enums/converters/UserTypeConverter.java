package com.telran.pillservice.clalitservice.model.entity.enums.converters;

import com.telran.pillservice.clalitservice.model.entity.enums.UserType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class UserTypeConverter implements AttributeConverter<UserType, Integer> {
    @Override
    public Integer convertToDatabaseColumn(UserType userType) {
        return userType == null ? null : userType.getId() ;
    }

    @Override
    public UserType convertToEntityAttribute(Integer dbData) {
        return dbData == null ? null : UserType.getById(dbData);
    }
}
