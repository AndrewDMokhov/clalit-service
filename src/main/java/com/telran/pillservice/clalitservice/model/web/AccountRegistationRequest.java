package com.telran.pillservice.clalitservice.model.web;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class AccountRegistationRequest {

    @NotBlank
    @Length(min = 3, max = 20)
    private String firstName;

    @NotBlank
    @Length(min = 2, max = 50)
    private String lastName;

    @NotNull
    @Size(min = 9,max = 9 )
    private String identificationNumber;

    @NotBlank
    @Length(min=5, max = 50)
    @Email
    private String email;

    @NotBlank
    @Length(min = 6, max = 50)
    private String password;

}
