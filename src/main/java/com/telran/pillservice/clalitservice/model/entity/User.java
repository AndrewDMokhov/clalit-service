package com.telran.pillservice.clalitservice.model.entity;

import com.telran.pillservice.clalitservice.model.base.AbstractBasePerson;

import lombok.*;


import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "USERS")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder

public class User extends AbstractBasePerson {

    @Column(name= "EMAIL")
    private String email;



}
