package com.telran.pillservice.clalitservice.model.entity;

import com.telran.pillservice.clalitservice.model.base.AbstractBaseEntity;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "USER_SESSION")
public class UserSession extends AbstractBaseEntity{

    @Column(name = "SESSION_ID", nullable = false, unique = true)
    private String sessionId;

    @ManyToOne
    @JoinColumn( name = "USER_ID", nullable = false, unique = false)
    private User user;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED_DATA", nullable = false, updatable = false)
    private Date createDate;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UPDATED_DATE")
    private Date updateDate;

    @Column(name = "IS_EXPIRED")
    private boolean isExpired;




}
