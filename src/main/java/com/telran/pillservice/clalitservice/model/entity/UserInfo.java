package com.telran.pillservice.clalitservice.model.entity;

import com.telran.pillservice.clalitservice.model.base.AbstractBaseEntity;
import com.telran.pillservice.clalitservice.model.entity.enums.Gender;
import com.telran.pillservice.clalitservice.model.entity.enums.converters.GenderConverter;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "USER_INFO")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class UserInfo extends AbstractBaseEntity {

    @OneToOne
    @JoinColumn(name = "ID", nullable = true)
    private User user;


    @Column(name = "GENDER")
    @Convert(converter = GenderConverter.class)
    private Gender gender;

    @Column(name = "BIRTHDAY")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateBirthday;

    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;




}
