package com.telran.pillservice.clalitservice.model.entity;

import com.telran.pillservice.clalitservice.model.base.AbstractBasePerson;
import lombok.*;

import javax.persistence.*;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "CHILDREN")
public class Child extends AbstractBasePerson {

    @ManyToOne
    @JoinColumn(name = "USER_PARENT_ID", nullable = true)
    private User userId;



}
