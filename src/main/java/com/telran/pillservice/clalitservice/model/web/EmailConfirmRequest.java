package com.telran.pillservice.clalitservice.model.web;

import com.telran.pillservice.clalitservice.model.entity.User;
import lombok.*;
import org.springframework.http.HttpStatus;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;


@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class EmailConfirmRequest {

    @NotBlank(message = "")
    private String link;

}
