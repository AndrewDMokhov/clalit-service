package com.telran.pillservice.clalitservice.model.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.telran.pillservice.clalitservice.model.base.AbstractBaseEntity;
import com.telran.pillservice.clalitservice.model.entity.enums.AccountStatus;
import com.telran.pillservice.clalitservice.model.entity.enums.AccountType;
import com.telran.pillservice.clalitservice.model.entity.enums.converters.AccountStatusConverter;
import com.telran.pillservice.clalitservice.model.entity.enums.converters.AccountTypeConverter;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "USER_ACCOUNT")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class UserAccount extends AbstractBaseEntity {

    @JoinColumn(name = "ID", nullable = false)
    @OneToOne
    private User user;

    @Column(name = "ACCOUNT_STATUS_ID")
    @Convert(converter = AccountStatusConverter.class)
    private AccountStatus accountStatus;

    @Column(name = "ACCOUNT_TYPE")
    @Convert(converter = AccountTypeConverter.class)
    private AccountType accountType;

}
