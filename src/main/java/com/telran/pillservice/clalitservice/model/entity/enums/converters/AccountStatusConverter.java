package com.telran.pillservice.clalitservice.model.entity.enums.converters;

import com.telran.pillservice.clalitservice.model.entity.enums.AccountStatus;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class AccountStatusConverter implements AttributeConverter<AccountStatus, Integer> {
    @Override
    public Integer convertToDatabaseColumn(AccountStatus accountStatus) {
        return accountStatus == null ? null : accountStatus.getId();
    }

    @Override
    public AccountStatus convertToEntityAttribute(Integer id) {
        return id == null ? null : AccountStatus.getById(id);
    }
}
