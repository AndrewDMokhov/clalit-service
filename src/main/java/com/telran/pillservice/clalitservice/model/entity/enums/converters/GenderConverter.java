package com.telran.pillservice.clalitservice.model.entity.enums.converters;

import com.telran.pillservice.clalitservice.model.entity.enums.Gender;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class GenderConverter implements AttributeConverter<Gender,Integer> {
    @Override
    public Integer convertToDatabaseColumn(Gender gender) {
        return gender == null ? Gender.INDEFINITE.getId() : gender.getId();
    }

    @Override
    public Gender convertToEntityAttribute(Integer id) {
        return id == null ? null : Gender.getById(id);
    }
}
