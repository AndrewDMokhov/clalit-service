package com.telran.pillservice.clalitservice.model.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class LoginRequest {


    private String identificationNum;
    private String password;
}
