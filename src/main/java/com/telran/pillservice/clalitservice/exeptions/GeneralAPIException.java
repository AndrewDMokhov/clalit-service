package com.telran.pillservice.clalitservice.exeptions;

import com.telran.pillservice.clalitservice.model.web.ErrorResponse;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
@NoArgsConstructor

public class GeneralAPIException extends AbstractAPIExeption {

    public static final String DEFAULT_MASSAGE="Unexpected error occurred";

    private String message;
    @Override
    public String getDefaultExeption() {
        if (message == null){
            return DEFAULT_MASSAGE;
        }
        return message;
    }

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }

    @Override
    public ErrorResponse getErrorResponse() {
        return ErrorResponse.builder()
                .message(getDefaultExeption())
                .build();
    }
}
