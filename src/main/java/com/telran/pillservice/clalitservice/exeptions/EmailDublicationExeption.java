package com.telran.pillservice.clalitservice.exeptions;

import com.telran.pillservice.clalitservice.model.web.ErrorResponse;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
@NoArgsConstructor
public class EmailDublicationExeption extends  GeneralAPIException{

    private static final String DEFAULT_MASSEGE = "Such email already exists: [%s]";
    private String email;

    @Override
    public String getDefaultExeption() {
        return String.format(DEFAULT_MASSEGE, email);
    }

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.BAD_REQUEST;
    }


}
