package com.telran.pillservice.clalitservice.exeptions;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
public class IdentificationNumDublExetion  extends GeneralAPIException{

    private static final String DEFAULT_MASSEGE = "Such ID number already exists";

    private String identificationNumber;

    @Override
    public String getDefaultExeption() {
        return  String.format(DEFAULT_MASSEGE, identificationNumber);
    }

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.BAD_REQUEST;
    }
}
