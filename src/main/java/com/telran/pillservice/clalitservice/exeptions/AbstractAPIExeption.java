package com.telran.pillservice.clalitservice.exeptions;

import com.telran.pillservice.clalitservice.model.web.ErrorResponse;
import org.springframework.http.HttpStatus;

public abstract class AbstractAPIExeption extends RuntimeException {

    public AbstractAPIExeption() {
    }

    public AbstractAPIExeption(String message) {
        super(message);
    }

    public abstract String getDefaultExeption();
    public abstract HttpStatus getStatus();
    public abstract ErrorResponse getErrorResponse();

}
