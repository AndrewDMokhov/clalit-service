package com.telran.pillservice.clalitservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClalitServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClalitServiceApplication.class, args);
    }
}
