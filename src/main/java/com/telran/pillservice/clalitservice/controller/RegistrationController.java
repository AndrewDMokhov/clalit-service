package com.telran.pillservice.clalitservice.controller;

import com.telran.pillservice.clalitservice.exeptions.GeneralAPIException;
import com.telran.pillservice.clalitservice.model.web.AccountRegistationRequest;
import com.telran.pillservice.clalitservice.model.web.EmailConfirmRequest;
import com.telran.pillservice.clalitservice.service.impl.RegistrationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.apache.commons.lang3.StringUtils;

import javax.validation.Valid;

@RestController
@RequestMapping("/user")
public class RegistrationController {

    @Autowired
    private RegistrationServiceImpl registrationService;

    @PostMapping(value = "/registration", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public void registration(@RequestBody @Valid AccountRegistationRequest accountRegistationRequest){
        registrationService.registerUser(accountRegistationRequest);
    }
    @RequestMapping(value = "/registration/confirm/{link}", method = RequestMethod.GET)
    public boolean confirmation (@PathVariable String link){
        if(StringUtils.isBlank(link)){
            throw new GeneralAPIException();
        }
        return registrationService.confirmValidation(link);
    }

}
