package com.telran.pillservice.clalitservice.repository;

import com.telran.pillservice.clalitservice.model.entity.UserSession;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserSessionRepository extends JpaRepository<UserSession, Long> {

}
