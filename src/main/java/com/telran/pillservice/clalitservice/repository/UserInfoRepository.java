package com.telran.pillservice.clalitservice.repository;

import com.telran.pillservice.clalitservice.model.entity.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserInfoRepository extends JpaRepository<UserInfo, Long> {
}
