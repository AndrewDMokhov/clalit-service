package com.telran.pillservice.clalitservice.repository;

import com.telran.pillservice.clalitservice.model.entity.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserAccountRepository extends JpaRepository<UserAccount, Long> {
}
