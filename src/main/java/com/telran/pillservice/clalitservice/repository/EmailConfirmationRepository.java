package com.telran.pillservice.clalitservice.repository;

import com.telran.pillservice.clalitservice.model.entity.EmailConfirmation;
import com.telran.pillservice.clalitservice.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailConfirmationRepository extends JpaRepository<EmailConfirmation, Long> {
    EmailConfirmation findByConfirmKey(String link);
}