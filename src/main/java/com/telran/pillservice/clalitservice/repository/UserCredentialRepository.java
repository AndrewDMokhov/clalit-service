package com.telran.pillservice.clalitservice.repository;

import com.telran.pillservice.clalitservice.model.entity.User;
import com.telran.pillservice.clalitservice.model.entity.UserCredential;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserCredentialRepository extends JpaRepository<UserCredential, Long> {
    UserCredential findByUser(User user);
}
