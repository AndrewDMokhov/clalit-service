package com.telran.pillservice.clalitservice.repository;

import com.telran.pillservice.clalitservice.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User getByUserIdentificationNumber(String numId);
    User findByEmail(String email);
}
