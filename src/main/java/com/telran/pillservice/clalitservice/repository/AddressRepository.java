package com.telran.pillservice.clalitservice.repository;

import com.telran.pillservice.clalitservice.model.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {
}
