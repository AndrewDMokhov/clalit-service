package com.telran.pillservice.clalitservice.repository;

import com.telran.pillservice.clalitservice.model.entity.Child;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChildRepository extends JpaRepository<Child, Long> {
}
