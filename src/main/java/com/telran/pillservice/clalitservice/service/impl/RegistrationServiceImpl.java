package com.telran.pillservice.clalitservice.service.impl;

import com.telran.pillservice.clalitservice.exeptions.EmailDublicationExeption;
import com.telran.pillservice.clalitservice.exeptions.IdentificationNumDublExetion;
import com.telran.pillservice.clalitservice.model.entity.User;
import com.telran.pillservice.clalitservice.model.entity.UserAccount;
import com.telran.pillservice.clalitservice.model.entity.UserInfo;
import com.telran.pillservice.clalitservice.model.entity.enums.AccountType;
import com.telran.pillservice.clalitservice.model.web.AccountRegistationRequest;
import com.telran.pillservice.clalitservice.model.web.EmailConfirmRequest;
import com.telran.pillservice.clalitservice.repository.UserAccountRepository;
import com.telran.pillservice.clalitservice.repository.UserInfoRepository;
import com.telran.pillservice.clalitservice.repository.UserRepository;
import com.telran.pillservice.clalitservice.service.EmailService;
import com.telran.pillservice.clalitservice.service.PasswordService;
import com.telran.pillservice.clalitservice.service.RegistationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class RegistrationServiceImpl implements RegistationService {

    private UserRepository userRepository;
    private UserAccountRepository userAccountRepository;
    private UserInfoRepository userInfoRepository;
    private PasswordService passwordService;
    private EmailService emailService;

    @Autowired
    RegistrationServiceImpl(
            UserRepository userRepository,
            UserAccountRepository userAccountRepository,
            UserInfoRepository userInfoRepository,
            PasswordService passwordService,
            EmailService emailService){
       this.userRepository = userRepository;
       this.userAccountRepository = userAccountRepository;
       this.userInfoRepository = userInfoRepository;
       this.passwordService = passwordService;
       this.emailService = emailService;
    }

    @Override
    public void registerUser(AccountRegistationRequest accountRegistationRequest) {
        User user = saveUser(accountRegistationRequest);
        saveUserAccount(user);
        saveUserInfo(user);
        passwordService.createAndSaveUserPassword(user,
                accountRegistationRequest.getPassword());
        userRepository.save(user);
        emailService.sendMessage(accountRegistationRequest.getEmail(),
                "Confirmation",
                emailService.confirmLinkGenerate(user)
                );
    }

    @Override
    public boolean confirmValidation(String link) {

        return emailService.validationLink(link);

    }

    private void saveUserInfo(User user) {
        UserInfo userInfo = new UserInfo();
        userInfo.setUser(user);
        userInfoRepository.save(userInfo);
    }

    private void saveUserAccount(User user) {
        UserAccount userAccount = new UserAccount();
        userAccount.setUser(user);
        userAccount.setAccountType(AccountType.REGULAR);
        userAccountRepository.save(userAccount);
    }

    private User saveUser(AccountRegistationRequest accountRegistationRequest) {
        User user = new User();
        user.setFirstName(accountRegistationRequest.getFirstName());
        user.setLastName(accountRegistationRequest.getLastName());
        String identificationNum = accountRegistationRequest.getIdentificationNumber();
        if(userRepository.getByUserIdentificationNumber(identificationNum) != null) {
            throw new IdentificationNumDublExetion(identificationNum);
        }
        user.setUserIdentificationNumber(identificationNum);
        String email = accountRegistationRequest.getEmail();
        if(userRepository.findByEmail(email) != null){
            throw new EmailDublicationExeption(email);
        }
        user.setEmail(email);
        userRepository.save(user);
        return user;
    }
}
