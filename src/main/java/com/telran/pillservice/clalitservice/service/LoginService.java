package com.telran.pillservice.clalitservice.service;

import com.telran.pillservice.clalitservice.model.web.LoginRequest;
import com.telran.pillservice.clalitservice.model.web.LoginResponse;
import com.telran.pillservice.clalitservice.model.web.RegisterRequest;

public interface LoginService {

    LoginResponse login(LoginRequest loginRequest);
    boolean logout();

}
