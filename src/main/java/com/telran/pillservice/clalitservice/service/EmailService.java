package com.telran.pillservice.clalitservice.service;

import com.telran.pillservice.clalitservice.model.entity.User;

public interface EmailService {
     void sendMessage (String to, String subject, String messageBody);
     String confirmLinkGenerate(User userId);
     boolean validationLink(String key);

}
