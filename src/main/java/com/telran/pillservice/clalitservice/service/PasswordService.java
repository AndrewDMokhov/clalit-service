package com.telran.pillservice.clalitservice.service;

import com.telran.pillservice.clalitservice.model.entity.User;

public interface PasswordService {

    void createAndSaveUserPassword(User user, String password);
    boolean isPasswordCorrect(User user, String password);
    void updatePasswordInDb(User user, String password);

}
