package com.telran.pillservice.clalitservice.service.impl;

import com.telran.pillservice.clalitservice.model.entity.User;
import com.telran.pillservice.clalitservice.model.web.LoginRequest;
import com.telran.pillservice.clalitservice.model.web.LoginResponse;
import com.telran.pillservice.clalitservice.repository.UserRepository;
import com.telran.pillservice.clalitservice.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;

public class LoginServiceImpl implements LoginService {


    UserRepository userRepository;

    @Override
    public LoginResponse login(LoginRequest loginRequest) {
        User user = userRepository.getByUserIdentificationNumber(loginRequest.getPassword());
        if( user == null) {
         throw new RuntimeException();
        }


        return null;
    }

    @Override
    public boolean logout() {
        return false;
    }
}
