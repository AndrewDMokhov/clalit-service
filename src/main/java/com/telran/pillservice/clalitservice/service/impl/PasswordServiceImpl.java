package com.telran.pillservice.clalitservice.service.impl;

import com.telran.pillservice.clalitservice.exeptions.GeneralAPIException;
import com.telran.pillservice.clalitservice.model.entity.User;
import com.telran.pillservice.clalitservice.model.entity.UserCredential;
import com.telran.pillservice.clalitservice.repository.UserCredentialRepository;
import de.mkammerer.argon2.Argon2;
import de.mkammerer.argon2.Argon2Factory;
import de.mkammerer.argon2.Argon2Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.SecureRandom;
import java.util.Base64;

@Service
@Transactional
public class PasswordServiceImpl implements com.telran.pillservice.clalitservice.service.PasswordService {

    private static final int SOLD_LEN = 20;
    private static final int MEMORY = 65536;
    private static final int PARALLELISM = 1;
    private static final int HASH_TIME = 2000;
    private static final String SALT_HARD = "!Ah.`Df8&6^o(%2@";

    private static int iterations;

    private Argon2 argon2;

    PasswordServiceImpl() {
        argon2 =Argon2Factory.create();

        iterations = Argon2Helper.findIterations(argon2, HASH_TIME,MEMORY,PARALLELISM);

    }

    @Autowired
    private UserCredentialRepository userCredentialRepository;

    @Override
    public void createAndSaveUserPassword(User user, String password) {
        UserCredential userCredential = new UserCredential();
        userCredential.setUser(user);
        String salt = generateSalt();
        userCredential.setPasswordSalt(salt);
        userCredential.setPasswordHash(generateHash(salt, password));
        userCredentialRepository.save(userCredential);

    }

   @Override
    public boolean isPasswordCorrect(User user, String password) {
        UserCredential userCredential = userCredentialRepository.findByUser(user);
        if (userCredential == null) {
            throw new GeneralAPIException("Password or login is not correct");
        }
        String salt = userCredential.getPasswordSalt();
        String hash = userCredential.getPasswordHash();
         return argon2.verify(hash, passwordFull(password, salt));
    }

    @Override
    public void updatePasswordInDb(User user, String password) {
        UserCredential userCredential = userCredentialRepository.findByUser(user);
        if(userCredential == null) {
            throw new GeneralAPIException("The User is not found ");
        }
        String salt = generateSalt();
        userCredential.setPasswordSalt(salt);
        userCredential.setPasswordHash(generateHash(salt,password));
        userCredentialRepository.save(userCredential);
    }

    private String passwordFull(String password, String salt) {
        return password + SALT_HARD + salt;
    }

    private String generateHash(String salt, String password) {
        return argon2.hash(iterations, MEMORY,PARALLELISM, passwordFull(password,salt));
    }

    private String generateSalt() {
        SecureRandom secureRandom = new SecureRandom();
        byte bytes[] = new byte[20];
        secureRandom.nextBytes(bytes);
        return Base64.getEncoder().encodeToString(bytes);
    }
}
