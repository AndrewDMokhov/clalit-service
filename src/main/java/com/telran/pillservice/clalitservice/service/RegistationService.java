package com.telran.pillservice.clalitservice.service;

import com.telran.pillservice.clalitservice.model.web.AccountRegistationRequest;
import com.telran.pillservice.clalitservice.model.web.EmailConfirmRequest;

public interface RegistationService {
        void registerUser(AccountRegistationRequest accountRegistationRequest);
        boolean confirmValidation(String link);
        }
