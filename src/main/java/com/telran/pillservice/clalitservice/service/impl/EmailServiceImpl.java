package com.telran.pillservice.clalitservice.service.impl;

import com.telran.pillservice.clalitservice.model.entity.EmailConfirmation;
import com.telran.pillservice.clalitservice.model.entity.User;
import com.telran.pillservice.clalitservice.model.entity.UserAccount;
import com.telran.pillservice.clalitservice.repository.EmailConfirmationRepository;
import com.telran.pillservice.clalitservice.repository.UserAccountRepository;
import com.telran.pillservice.clalitservice.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

@Service
public class EmailServiceImpl implements EmailService {

    @Value("${domain.name}")
    private String url;

    private JavaMailSender emailSender;
    private UserAccountRepository userAccountRepository;

    @Autowired
    EmailServiceImpl(JavaMailSender emailSender,
                     UserAccountRepository userAccountRepository){
        this.emailSender = emailSender;
        this.userAccountRepository = userAccountRepository;

    }

    @Autowired
    private EmailConfirmationRepository emailConfirmationRepository;


    @Override
    public void sendMessage(String to, String subject, String messageBody) {

        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(subject);
        message.setText(messageBody);
        emailSender.send(message);

    }

    @Override
    public String confirmLinkGenerate(User user) {

        String key = UUID.randomUUID().toString();
        EmailConfirmation emailConfirmation = EmailConfirmation.builder().
                user(user).
                confirmKey(key).
                confirmCreateTime(new Date())
                .build();
        emailConfirmationRepository.save(emailConfirmation);

        return  "http://" + url + "/user/registration/confirmation" + "/" + key ;
    }

    @Override
    public boolean validationLink(String link) {
        EmailConfirmation emailConfirm = emailConfirmationRepository.findByConfirmKey(link);
        if(emailConfirm == null) {
            throw new RuntimeException();
        }

        emailConfirmationRepository.delete(emailConfirm);
        return true;
    }
}
